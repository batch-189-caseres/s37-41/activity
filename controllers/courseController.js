const Course = require('../models/Course');
const User = require('../models/User');

//Create a new course
module.exports.addCourse = (reqBody) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	// Saves the created object to our database
	return newCourse.save().then((course, error) => {

		// Course creation failed
		if(error) {

			return false

		// Course creation successful
		} else {

			return true
		}
	})
}

//Activity Solution 2
// module.exports.addCourse = (reqBody, userData) => {

//     return User.findById(userData.userId).then(result => {

//         if (userData.isAdmin == false) {
//             return "You are not an admin"
//         } else {
//             let newCourse = new Course({
//                 name: reqBody.name,
//                 description: reqBody.description,
//                 price: reqBody.price
//             })
        
//             //Saves the created object to the database
//             return newCourse.save().then((course, error) => {
//                 //Course creation failed
//                 if(error) {
//                     return false
//                 } else {
//                     //course creation successful
//                     return "Course creation successful"
//                 }
//             })
//         }
        
//     });    
// }


//Retrieve All Courses
module.exports.getAllCourses = async (data) => {

	if (data.isAdmin) {
		return Course.find({}).then(result => {

			return result
		})
	} else {

		return false
	}


}


module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})

}


module.exports.getCourse = (reqParams) => {


	return Course.findById(reqParams.courseId).then(result => {

		return result
	})

}


// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	}

	// Syntax
			// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) =>{

		// Course not updated
		if(error) {

			return false

		// Course updated successfully
		} else {

			return true
		
		}


	})


}

module.exports.deActivateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let deActivateCourse = {

		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, deActivateCourse).then((course, error) =>{

		// Course not updated
		if(error) {

			return false

		// Course updated successfully
		} else {

			return true
		
		}


	})
}
